# ELCM

![ELCM logo](logo.png)

ELCM Living Campaign Manager is a tool designed for living campaign communities. The goal is to eliminate the hodgepodge of systems used to manage a living campaign (a wiki, a forum, a messager etc) and condense them to just ELCM.

We are currently in pre-alpha and looking for help, please let us know if you'd be interested!

